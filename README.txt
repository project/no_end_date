No End Date
===========

Extends Date Popup widget: provides an option to hide the date input for the end date
(the end time input is visible). It might be useful when one needs to set a single date,
start time and end time (e.g. for an event that takes place during a single day).

After submitting the field the end date is set to the same value as the start date, i.e.
the end datetime is correct and can be used as if the end date

The respective checkbox (Hide end date) is located under the advanced settings of widget form.
